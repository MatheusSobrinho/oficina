\beamer@endinputifotherversion {3.24pt}
\select@language {brazil}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Iniciando}{4}{0}{1}
\beamer@sectionintoc {2}{Comandos iniciais}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Primeiro documento}{12}{0}{2}
\beamer@sectionintoc {3}{Expandindo o \textlatin {\LaTeX }}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{Projetos com v\IeC {\'a}rios arquivos}{35}{0}{3}
\beamer@sectionintoc {4}{Bibliografia}{42}{0}{4}
\beamer@subsectionintoc {4}{1}{BibTEX}{43}{0}{4}
\beamer@sectionintoc {5}{\textlatin {\LaTeX }$ \ $ na Internet}{48}{0}{5}
\beamer@subsectionintoc {5}{1}{Editores Online}{49}{0}{5}
